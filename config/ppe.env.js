'use strict'
module.exports = {
  NODE_ENV: '"production"',
  ROOT_API: '"https://www.europeandataportal.eu/api"',
  USERNAME_ENV: '"xxxxxxxxxx"',
  PASSWORD_ENV: '"xxxxxxxxxxxxx"'
}

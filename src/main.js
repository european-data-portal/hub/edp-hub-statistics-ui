// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import './setup/runtimeconfig'
import App from './App'
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import SubNav from './components/SubNav.vue'
import Evolution from './components/Evolution.vue'
import ChartContainer from './components/ChartContainer.vue'
import PieContainer from './components/PieContainer.vue'
import VueI18n from 'vue-i18n'
import _ from 'lodash'
import Sticky from 'vue-sticky-directive'
import Countries from './components/Countries.vue'
import CountryAndCatalogue from './components/CountryAndCatalogue.vue'
import ChartEvolutionCat from './components/ChartEvolutionCat.vue'
import ChartEvolutionCountry from './components/ChartEvolutionCountry.vue'
import ChartEvolutionCatalogues from './components/ChartEvolutionCatalogues.vue'
import ChartEvolutionTotal from './components/ChartEvolutionTotal.vue'
import i18njson from './i18n/lang.json'

// Import Font Awesome Icons Library for vue
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faGoogle,
  faGooglePlus,
  faGooglePlusG,
  faFacebook,
  faFacebookF,
  faInstagram,
  faTwitter,
  faLinkedinIn
} from '@fortawesome/free-brands-svg-icons'
import {
  faComment,
  faExternalLinkAlt,
  faPlus,
  faMinus,
  faArrowDown,
  faArrowUp,
  faInfoCircle
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

require('bootstrap')
require('./styles/styles.sass')

Vue.prototype.$_ = _

require('./assets/img/edplogo.png')

Vue.config.productionTip = false

library.add(faGoogle, faGooglePlus, faGooglePlusG, faFacebook, faFacebookF, faInstagram, faTwitter, faLinkedinIn, faComment, faExternalLinkAlt, faPlus, faMinus, faArrowDown, faArrowUp, faInfoCircle)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueI18n)
Vue.use(Sticky)

Vue.component('sub-nav', SubNav)
Vue.component('Evolution', Evolution)
Vue.component('chart-container', ChartContainer)
Vue.component('pie-container', PieContainer)
Vue.component('countries', Countries)
Vue.component('country-catalogue', CountryAndCatalogue)
Vue.component('chart-evolution-cat', ChartEvolutionCat)
Vue.component('chart-evolution-cauntry', ChartEvolutionCountry)
Vue.component('chart-evolution-catalogue', ChartEvolutionCatalogues)
Vue.component('chart-evolution-total', ChartEvolutionTotal)

const i18n = new VueI18n({
  locale: 'en', // set locale
  messages: i18njson // set locale messages
})

/* eslint-disable no-new */
new Vue({
  i18n,
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

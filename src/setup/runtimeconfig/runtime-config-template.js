/**
 * Configuration template file to bind specific properties to environment variables.
 * All values must have the prefix $VUE_APP_.
 * Their corresponding environment variable key labels must be the values without the $ character.
 * This object should be structurally identical (name and path) to the standard configuration file.
 */
export default {
  ROOT_API: '$VUE_APP_ROOT_API',
  BASE_PATH: '$VUE_APP_BASE_PATH',
  MATOMO_URL: '$VUE_APP_MATOMO_URL'
}

import Vue from 'vue'
import Router from 'vue-router'
import SubNav from '@/components/SubNav'
import Evolution from '@/components/Evolution'
import CurrentState from '@/components/CurrentState'

Vue.use(Router)

export default new Router({
  mode: 'history', // disables component hashtag in url (# > Anchor now)
  base: Vue.prototype.$env.BASE_PATH,
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      redirect: '/catalogue-statistics'
    },
    {
      path: '/SubNav',
      name: 'SubNav',
      component: SubNav
    },
    {
      path: '/Evolution',
      name: 'Evolution',
      component: Evolution
    },
    {
      path: '/CurrentState',
      name: 'CurrentState',
      component: CurrentState
    }
  ]
})

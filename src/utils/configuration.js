export default class Configuration {
  static get CONFIG () {
    return ({
      ROOT_API: '$VUE_APP_ROOT_API',
      MATOMO_URL: '$VUE_APP_MATOMO_URL'
    })
  }

  static value (name) {
    if (!(name in this.CONFIG)) {
      console.log(`Configuration: There is no key named "${name}"`)
      return
    }

    const value = this.CONFIG[name]

    if (!value) {
      console.log(`Configuration: Value for "${name}" is not defined`)
      return
    }

    if (value.startsWith('$VUE_APP_')) {
      // value was not replaced, it seems we are in development.
      // Remove $VUE_APP_ and get current value from process.env
      const envValue = process.env[name]
      if (envValue) {
        return envValue
      } else {
        console.log(`Configuration: Environment variable "${name}" is not defined`)
      }
    } else {
      // value was already replaced, it seems we are in production.
      return value
    }
  }
}
